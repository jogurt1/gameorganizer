#include <QFileDialog>
#include <QPushButton>
#include <QShortcut>
#include <QtGui>
#include <fstream>
#include <nlohmann/json.hpp>

#include "./ui_mainwindow.h"
#include "Game.h"
#include "mainwindow.h"
using json = nlohmann::json;

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent), ui(new Ui::MainWindow) {
  gameForm = new GameForm(this);
  addGameButton = new QPushButton("Add Game", this);
  new QShortcut(QKeySequence(Qt::CTRL | Qt::Key_Q), this, SLOT(close()));

  ui->setupUi(this);
  ui->mainLayout->addWidget(gameForm);
  ui->mainLayout->addWidget(addGameButton);
  connect(ui->loadFileButton, SIGNAL(clicked()), this, SLOT(selectFile()));
  connect(addGameButton, SIGNAL(clicked()), this, SLOT(addGame()));
}

MainWindow::~MainWindow() { delete ui; }

void MainWindow::setFileName(const QString &name) {
  ui->fileNameTextInput->setText(name);
}

QString MainWindow::fileName() const { return ui->fileNameTextInput->text(); }

void MainWindow::addGame() {
  gamesList.addGame(gameForm->generateGame());
  organizeGames();
  gameForm->clear();
}

void MainWindow::selectFile() {
  QString fileName = QFileDialog::getOpenFileName(
      this, tr("Open Games List"), "games.json", tr("JSON (*json)"));
  setFileName(fileName);
  readGamesList();
}

void MainWindow::organizeGames() {
  gamesList.sort();
  gamesList.countConsoles();
  gamesList.countYears();
  gamesList.countGames();
  gamesList.writeFiles();
}

void MainWindow::readGamesList() {
  std::ifstream file(MainWindow::fileName().toStdString());
  json data = json::parse(file);
  file.close();

  for (auto &element : data) {
    RyoHazuki::Game game;
    game.loadFromJson(element);
    gamesList.addGame(game);
  }
  organizeGames();
}
