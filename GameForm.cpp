#include "GameForm.h"
#include "Game.h"

#include <QIntValidator>
#include <QWidget>
#include <qnamespace.h>

GameForm::GameForm(QWidget *parent) {
  layout = new QFormLayout(parent);
  layout->setHorizontalSpacing(20);
  layout->setVerticalSpacing(30);

  titleLabel = new QLabel(tr("Title"));
  consoleLabel = new QLabel(tr("Console"));
  regionLabel = new QLabel(tr("Region"));
  publisherLabel = new QLabel(tr("Publisher"));
  genreLabel = new QLabel(tr("Genre"));
  yearLabel = new QLabel(tr("Year"));
  quantityLabel = new QLabel(tr("Quantity"));
  boxLabel = new QLabel(tr("In Box"));
  manualLabel = new QLabel(tr("Manual"));
  titleInput = new QLineEdit;
  consoleInput = new QLineEdit;
  regionInput = new QLineEdit;
  publisherInput = new QLineEdit;
  genreInput = new QLineEdit;
  yearInput = new QLineEdit;
  quantityInput = new QLineEdit;
  boxInput = new QLineEdit;
  manualInput = new QLineEdit;
  titleInput->setPlaceholderText(tr("Game Title"));
  titleInput->setMaxLength(128);
  consoleInput->setPlaceholderText(tr("Console"));
  consoleInput->setMaxLength(128);
  regionInput->setPlaceholderText(tr("Region"));
  regionInput->setMaxLength(64);
  publisherInput->setPlaceholderText(tr("Publisher"));
  publisherInput->setMaxLength(64);
  genreInput->setPlaceholderText(tr("Genre"));
  genreInput->setMaxLength(64);
  yearInput->setPlaceholderText(tr("Year"));
  yearInput->setMaxLength(64);
  quantityInput->setPlaceholderText(tr("Quantity"));
  quantityInput->setMaxLength(3);
  quantityInput->setValidator(new QIntValidator(0, 10));
  boxInput->setPlaceholderText(tr("In Box"));
  boxInput->setMaxLength(3);
  boxInput->setValidator(new QIntValidator(0, 10));
  manualInput->setPlaceholderText(tr("Manual"));
  manualInput->setMaxLength(3);
  manualInput->setValidator(new QIntValidator(0, 10));
  layout->addRow(titleLabel, titleInput);
  layout->addRow(consoleLabel, consoleInput);
  layout->addRow(regionLabel, regionInput);
  layout->addRow(publisherLabel, publisherInput);
  layout->addRow(genreLabel, genreInput);
  layout->addRow(yearLabel, yearInput);
  layout->addRow(quantityLabel, quantityInput);
  layout->addRow(boxLabel, boxInput);
  layout->addRow(manualLabel, manualInput);

  setLayout(layout);
}

GameForm::~GameForm() {
  delete titleLabel;
  delete consoleLabel;
  delete regionLabel;
  delete publisherLabel;
  delete genreLabel;
  delete yearLabel;
  delete quantityLabel;
  delete boxLabel;
  delete manualLabel;
  delete titleInput;
  delete consoleInput;
  delete regionInput;
  delete publisherInput;
  delete genreInput;
  delete yearInput;
  delete quantityInput;
  delete boxInput;
  delete manualInput;
  delete layout;
}

RyoHazuki::Game GameForm::generateGame() {
  return RyoHazuki::Game(
      titleInput->text().toStdString(), consoleInput->text().toStdString(),
      regionInput->text().toStdString(), publisherInput->text().toStdString(),
      genreInput->text().toStdString(), yearInput->text().toStdString(),
      std::stoi(quantityInput->text().toStdString()),
      std::stoi(boxInput->text().toStdString()),
      std::stoi(manualInput->text().toStdString()));
}

void GameForm::clear() {
  titleInput->clear();
  consoleInput->clear();
  regionInput->clear();
  publisherInput->clear();
  genreInput->clear();
  yearInput->clear();
  quantityInput->clear();
  boxInput->clear();
  manualInput->clear();
  titleInput->setFocus();
}