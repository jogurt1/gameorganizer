#include <string>
#include "Game.h"

void RyoHazuki::Game::loadFromJson(json j)
{
    Title = j["Title"].template get<std::string>();
    Console = j["Console"].template get<std::string>();
    Region = j["Region"].template get<std::string>();
    Publisher = j["Publisher"].template get<std::string>();
    Genre = j["Genre"].template get<std::string>();
    Year = j["Year"].template get<std::string>();
    Quantity = j["Quantity"].template get<int>();
    Box = j["Box"].template get<int>();
    Manual = j["Manual"].template get<int>();
}

json RyoHazuki::Game::toJson()
{
    return json{
        {"Console", Console},
        {"Title", Title},
        {"Region", Region},
        {"Publisher", Publisher},
        {"Genre", Genre},
        {"Year", Year},
        {"Quantity", Quantity},
        {"Box", Box},
        {"Manual", Manual}
    };
}

std::string RyoHazuki::Game::getConsole() const
{
    return Console;
}

std::string RyoHazuki::Game::getPublisher() const
{
    return Publisher;
}

std::string RyoHazuki::Game::getTitle() const
{
    return Title;
}

std::string RyoHazuki::Game::getYear() const
{
    return Year;
}
