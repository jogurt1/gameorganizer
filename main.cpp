#include "mainwindow.h"

#include <QApplication>
#include <nlohmann/json.hpp>
using json = nlohmann::json;


int main(int argc, char *argv[])
{
    QApplication application(argc, argv);
    MainWindow w;
    w.show();
    return application.exec();
}
