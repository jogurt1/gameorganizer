#include <algorithm>
#include <fstream>
#include <iostream>
#include <string>
#include <unordered_map>
#include <vector>

#include "Game.h"
#include "GamesList.h"

void RyoHazuki::GamesList::addGame(RyoHazuki::Game game) {
  // gamesList[game.getConsole()].push_back(game);
  gamesList.push_back(game);
  std::cout << game.getTitle() << std::endl;
  if (years.contains(game.getYear())) {
    years[game.getYear()]++;
  } else {
    years[game.getYear()] = 1;
  }
  if (consoles.contains(game.getConsole())) {
    consoles[game.getConsole()]++;
  } else {
    consoles[game.getConsole()] = 1;
  }
  if (publishers.contains(game.getPublisher())) {
    publishers[game.getPublisher()]++;
  } else {
    publishers[game.getPublisher()] = 1;
  }
}

void RyoHazuki::GamesList::sort() {
  std::cout << "Sorting titles " << gamesList.size() << std::endl;
  consoleNames.clear();
  consoleNames = buildVector(consoles);
  std::sort(gamesList.begin(), gamesList.end(),
            [](RyoHazuki::Game const &a, RyoHazuki::Game &b) {
              if (a.getConsole().compare(b.getConsole()) != 0) {
                return a.getConsole().compare(b.getConsole()) < 0;
              }
              if (a.getTitle().compare(b.getTitle()) != 0) {
                return a.getTitle().compare(b.getTitle()) < 0;
              }
              return false;
            });
  std::cout << "Done" << std::endl;
}

void RyoHazuki::GamesList::countConsoles() {
  for (const auto &[console, count] : consoles) {
    std::cout << console << ",\t" << count << std::endl;
  }
}

void RyoHazuki::GamesList::countYears() {
  for (const auto &[year, count] : years) {
    std::cout << year << ",\t" << count << std::endl;
  }
}

void RyoHazuki::GamesList::countGames() {
  int numberOfGames = 0;
  for (const auto &[console, count] : consoles) {
    numberOfGames += count;
  }
  std::cout << "Number of games: " << numberOfGames << std::endl;
}

json RyoHazuki::GamesList::buildJson(std::vector<std::string> keys,
                                     std::unordered_map<std::string, int> map,
                                     std::string typeCounted) {
  json output;
  for (const auto &key : keys) {
    output.push_back({{typeCounted, key}, {"Count", map[key]}});
  }
  return output;
}

std::vector<std::string>
RyoHazuki::GamesList::buildVector(std::unordered_map<std::string, int> map) {
  std::vector<std::string> outputVector;
  for (const auto &[key, value] : map) {
    outputVector.push_back(key);
  }
  std::sort(outputVector.begin(), outputVector.end());
  return outputVector;
}

void RyoHazuki::GamesList::writeOutputFile(json data, std::string filename) {
  std::ofstream outputFile(filename);
  outputFile << std::setw(4) << data << std::endl;
  outputFile.close();
}

void RyoHazuki::GamesList::writeFiles() {
  json output;
  json outputConsoles;
  json outputPublishers;
  json outputYears;
  std::vector<std::string> publishersVector = buildVector(publishers);
  std::vector<std::string> yearsVector = buildVector(years);

  for (auto &game : gamesList) {
    std::cout << game.getTitle() << std::endl;
    output.push_back(game.toJson());
  }

  outputConsoles = buildJson(consoleNames, consoles, "Console");
  outputPublishers = buildJson(publishersVector, publishers, "Publisher");
  outputYears = buildJson(yearsVector, years, "Year");

  writeOutputFile(output, "output/games.json");
  writeOutputFile(outputConsoles, "output/consoles.json");
  writeOutputFile(outputPublishers, "output/publishers.json");
  writeOutputFile(outputYears, "output/years.json");
}
