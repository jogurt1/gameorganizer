#ifndef GAME_H
#define GAME_H
#include <nlohmann/json.hpp>
#include <string>
using json = nlohmann::json;

namespace RyoHazuki {
class Game {
public:
  Game() {};
  Game(std::string title, std::string console, std::string region,
       std::string publisher, std::string genre, std::string year, int quantity,
       int box, int manual)
      : Title(title), Console(console), Region(region), Publisher(publisher),
        Genre(genre), Year(year), Quantity(quantity), Box(box), Manual(manual) {
  }
  void loadFromJson(json j);
  json toJson();
  std::string getConsole() const;
  std::string getPublisher() const;
  std::string getTitle() const;
  std::string getYear() const;

private:
  std::string Console;
  std::string Region;
  std::string Title;
  std::string Publisher;
  std::string Genre;
  std::string Year;
  int Quantity;
  int Box;
  int Manual;
};
} // namespace RyoHazuki

#endif // GAME_H
