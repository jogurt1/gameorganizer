#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QPushButton>

#include "GameForm.h"
#include "GamesList.h"

QT_BEGIN_NAMESPACE
namespace Ui {
class MainWindow;
}
QT_END_NAMESPACE

class MainWindow : public QMainWindow {
  Q_OBJECT

public:
  MainWindow(QWidget *parent = nullptr);
  ~MainWindow();

  QString fileName() const;
  void setFileName(const QString &name);

private:
  void organizeGames();

  Ui::MainWindow *ui;
  GameForm *gameForm;
  QPushButton *addGameButton;
  RyoHazuki::GamesList gamesList;

private slots:
  void addGame();
  void selectFile();
  void readGamesList();
};
#endif // MAINWINDOW_H
