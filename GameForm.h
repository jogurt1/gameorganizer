#pragma once

#include "Game.h"
#include <QFormLayout>
#include <QLabel>
#include <QLineEdit>
#include <QString>
#include <QWidget>

class GameForm : public QWidget {
public:
  GameForm(QWidget *parent);
  ~GameForm();
  RyoHazuki::Game generateGame();
  void clear();

private:
  QFormLayout *layout;
  QLabel *titleLabel;
  QLabel *consoleLabel;
  QLabel *regionLabel;
  QLabel *publisherLabel;
  QLabel *genreLabel;
  QLabel *yearLabel;
  QLabel *quantityLabel;
  QLabel *boxLabel;
  QLabel *manualLabel;
  QLineEdit *titleInput;
  QLineEdit *consoleInput;
  QLineEdit *regionInput;
  QLineEdit *publisherInput;
  QLineEdit *genreInput;
  QLineEdit *yearInput;
  QLineEdit *quantityInput;
  QLineEdit *boxInput;
  QLineEdit *manualInput;
};
