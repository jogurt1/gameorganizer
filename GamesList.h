#pragma once

#include "Game.h"
#include <QWidget>
#include <nlohmann/json.hpp>
#include <string>
#include <unordered_map>
#include <vector>

using json = nlohmann::json;

namespace RyoHazuki {
class GamesList {
public:
  void addGame(Game game);
  void sort();
  void countConsoles();
  void countGames();
  void countYears();
  void writeFiles();

private:
  std::vector<std::string> buildVector(std::unordered_map<std::string, int>);
  json buildJson(std::vector<std::string>, std::unordered_map<std::string, int>, std::string);
  void writeOutputFile(json, std::string);

  std::unordered_map<std::string, int> consoles;
  std::vector<std::string> consoleNames;
  std::vector<Game> gamesList; //std::unordered_map<std::string, std::vector<Game>> gamesList;
  std::unordered_map<std::string, int> publishers;
  std::unordered_map<std::string, int> years;
};
} // namespace RyoHazuki
